import mongoose from 'mongoose';

const User = new mongoose.Schema({
    name :{
        type: 'string',
        required: true
    },
    team :{
        type: 'string',
        required: true
    }
})