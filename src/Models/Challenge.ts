export const Challenges = [
        {subject: "basic challenge", name: 'a', needToKnow: ['socket', 'tcp', 'more...'], body: 'b', id: 1, server: 'localhost', port: 3000},
        {subject: "experts", name: 'c', needToKnow: ['socket', 'tcp', 'more...'], body: 'd', id: 2, server: 'localhost', port: 3000},
        {subject: "basic challenge", name: 'c', needToKnow: ['socket', 'tcp', 'more...'], body: 'd', id: 2, server: 'localhost', port: 3000},
        {subject: "basic challenge", name: '1', needToKnow: ['socket', 'tcp', 'more...'], body: '2', id: 3, server: 'localhost', port: 3000},
        {subject: "basic challenge", name: '3', needToKnow: ['socket', 'tcp', 'more...'], body: '4', id: 4, server: 'localhost', port: 3000}
    ]

export const CompletedChallenges = {
    "amir" : [1, 4]
}