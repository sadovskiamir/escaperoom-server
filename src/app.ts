import express from 'express';

var challengeRouter = require('./Routers/Challenge');

var cors = require('cors')

const app = express();
const port = 8080;

app.use(cors())

app.use('/challenge', challengeRouter)

app.listen(port, () => {
  console.log(`Timezones by location application is running on port ${port}.`);
});