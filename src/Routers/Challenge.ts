import { Challenges, CompletedChallenges } from "../Models/Challenge";

var router = require('express').Router();

router.get('/', function(req, res, next){
     return res.send(Challenges);
  })

  
router.get('/Completed/:Name/:ChallengeId', function(req, res, next){
  return res.send(CompletedChallenges[req.params.Name].includes(parseInt(req.params.ChallengeId)));
})

module.exports = router;